/* File: gulpfile.js */

var gulp         = require('gulp');
var jade         = require('gulp-jade');
var less         = require('gulp-less');
var coffee       = require('gulp-coffee');
var concat       = require('gulp-concat');
var gutil        = require('gulp-util');
var express      = require('express');
var tinylr       = require('tiny-lr');
var flatten      = require('gulp-flatten');
var uglify       = require('gulp-uglify');
var packageFile  = require('./package.json');

//Handle COFFEE files
gulp.task('compile_coffee', function() {
  return gulp.src('src/**/*.coffee')
   .pipe(coffee({ bare: true }))
   .pipe(concat('app.js'))
   .pipe(gulp.dest('./_public/includes/js/'));
});

// Handle LESS files
gulp.task('compile_less', function() {
  return gulp.src('src/**/*.less')
    .pipe(less())
    .pipe(concat('style.css'))
    .pipe(gulp.dest('_public/includes/css/'));
});

// Handle JADE files
gulp.task('compile_index', function() {
  return gulp.src('src/index.jade')
    .pipe(jade())
    .pipe(gulp.dest('_public/'));
});

gulp.task('compile_jade', function() {
  return gulp.src('src/app-modules/**/*.jade')
    .pipe(jade())
    .pipe(flatten())
    .pipe(gulp.dest('_public/views/'));
});

// Handles all vendor files from dependencies section in gulpfile
gulp.task('create_vendor', function() {
  var excludedFiles = Object.keys(packageFile.devDependencies);
  var includedFiles  = Object.keys(packageFile.dependencies);
  excludedFiles.forEach(function( vendorFile, fileIndex ){
    excludedFiles[fileIndex] = "!node_modules/" + vendorFile + '/**/*.*';
  });
  includedFiles.forEach(function( libraryFile, fileIndex ){
    includedFiles[fileIndex] = 'node_modules/' + libraryFile + '/**/*.min.js';
  });
  var allFiles = includedFiles.concat(excludedFiles);
  return gulp.src(allFiles)
             .pipe(flatten())
             .pipe(concat('vendor.js'))
             .pipe(uglify())
             .pipe(gulp.dest('./_public/includes/vendor/'));
});

// Handle RAW files
gulp.task('copy_raw', function() {
  return gulp.src(['src/raw_data/**'])
        .pipe(gulp.dest('./_public/includes'));
});

// Start NODE server
// gulp.task('start_server', function() {
//   app = express();
//   app.use(require('connect-livereload')({port: 8934}));
//   app.use(express.static('./_public/'));
//   app.engine('html', require('ejs').renderFile);
//   app.set('view engine', 'html');
//   app.set('views', __dirname);

//   app.get('/', function(req, res){
//     res.render('_public/index.html');
//   });
//   app.listen(process.env.PORT || 4002);

// });

gulp.task('livereload', function() {
  app = tinylr();
  app.listen(8934);
});

gulp.task('watch_dog', function() {
  gulp.watch('raw_data/**', ['copy_raw']);
  gulp.watch('src/**/*.coffee', ['compile_coffee']);
  gulp.watch('src/app-modules/**/*.jade', ['compile_jade']);
  gulp.watch('src/index.jade', ['compile_index']);
  gulp.watch('src/**/*.jade', ['compile_jade']);
  gulp.watch('src/**/*.less', ['compile_less']);
  gulp.watch('index.jade', ['compile_index']);
});

gulp.task('default', ['compile_less',
                      'compile_index',
                      'compile_jade',
                      'copy_raw',
                      'create_vendor',
                      'compile_coffee',
                      //'start_server',
                      'livereload',
                      'watch_dog']);